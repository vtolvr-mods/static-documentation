# Welcome to the VTOL VR Mods Documentation!

## [Back to main site](https://vtolvr-mods.com/)

Here you can find information on how to use the Mod Loader and modding in VTOL VR.

If you would like to contribute to this documentation it is open source. You can find the repository [here](https://gitlab.com/vtolvr-mods/static-documentation)

## Useful links
- [Steam Store Page](https://store.steampowered.com/app/3018410)
- [Steam Workshop Page](https://steamcommunity.com/app/3018410/workshop/)
- [Creating a mod](/creating-a-mod/index.html)
- [Other documentation](/documentation/index.html)
- [dnSpy](https://github.com/dnSpyEx/dnSpy/releases/latest)

dnSpy is used to view the games code.
- [Asset Studio](https://github.com/Perfare/AssetStudio)

Asset Studio is used for exploring and extracting game assets
- [HarmonyX Wiki](https://github.com/BepInEx/HarmonyX/wiki)

HarmonyX is used for getting private variables, running code before in-game methods, and much more.