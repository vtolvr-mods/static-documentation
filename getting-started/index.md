# How to install the Mod Loader

[The Mod Loader is now available on Steam](https://store.steampowered.com/app/3018410), you can install it like any other Steam game. By heading over to the store page and pressing "Install now", make sure to install it on the same drive as VTOL VR otherwise it will be unable to find the game.

![Steam Page with install now button showing](/images/getting-started/steam%20page%20install%20button.png)

# How to uninstall the Mod Loader

You can just uninstall it from Steam and it will delete the necessary files. 

![Steam Library showing the uninstall button](/images/getting-started/uninstalling.png)

The only left over files will be:
- Logs
    - `steamapps\common\VTOL VR\@Mod Loader\Logs`
    - `steamapps\common\VTOL VR\@Mod Loader\Mod Manager\Logs`
    - `steamapps\common\VTOL VR\@Mod Loader\Mod Uploader\Logs`
- doorstop_config.ini
    - `steamapps\common\VTOL VR\doorstop_config.ini`
- Local mods you've created
    - `steamapps\common\VTOL VR\@Mod Loader\Mods`

# How to launch the game with mods

Launch the Mod Loader and in the top right you should see a "Play" button. This will launch the game up with mods

![Mod Loader with play button highlighted](/images/getting-started/launching%20modded.png)

# How to launch the game without mods

You can launch VTOL VR from Steam like how you usually would.

The Mod Loader does not modify the game files any more, so there is no need to verify your game files.