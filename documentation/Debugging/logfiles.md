# The Log Files

`Player.log` contains the log messages of the last play session. *This is useful to tell in game issues such as a mod not working or skin not loading.*

`Info.txt` contains generic information about what mods and skins are installed, where the mod loader is installed to. *This is useful if they are struggling to even see the mods button in game.*

`Launcher Log.txt` is the text from the Mod Loader's exe, *this is only useful for when there is a crash with the program its self.* 

`Log VTPatcher.txt` is what modifies the games code before launching, so that the Mod Loader can load. *The main issue from this is if the file doesn't exist at all, it shows that doorstop isn't running correctly so the Mod Loader can't get into the game.*

> [!TIP]
> Ignore these errors, these are normal.  
>`[1/21/2023 5:05:30 PM](Core: Error) Couldn't find folder \My Mods`  
>`[1/21/2023 5:05:30 PM](Core: Error) Couldn't find folder \My Skins`

> [!NOTE]
> Even if you believe you are able to fix the problem yourself it is best to go to the [VTOL VR Modding Discord](https://discord.gg/XZeeafp) and ask for help in the [Modding Help Channel](https://discord.com/channels/597153468834119710/1020419698594172978)