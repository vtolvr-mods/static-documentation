# What is the Flight Scene?
The flight scene is the scene where you can fly your airplane, typically the AV-42C, F/A-26B, or the F45A. It's useful to wait for this scene if you want to only run code when the scene is Active. There are 3 scenes that constitute as a "flight" scene, Akutan, CustomMapBase, and OverCloud_CustomMapBase.

# How to wait for the flight scene
The first thing you want to do is write a scene loaded function. The SceneLoaded function should look something like this, 
```cs
 private void SceneChanged(VTOLScenes scenes) // You can name it anything, naming it SceneLoaded will cause unity to call it and could break some things
    {
        if (scenes == VTOLScenes.Akutan || scenes == VTOLScenes.CustomMapBase || scenes == VTOLScenes.CustomMapBase_OverCloud) // If inside of a scene that you can fly in
        {
            // Your code here.
        }
    }
```
This code will cause whatever is inside of the if statement to be ran when a flightscene starts, however this can also cause code to run before objects (like the player's vehicle) are loaded in, to combat this we need to write a [coroutine]("https://docs.unity3d.com/Manual/Coroutines.html") to make sure we don't run code before the scene is loaded.
```cs
private IEnumerator waitForLoad()
    {
        while (VTMapManager.fetch == null || !VTMapManager.fetch.scenarioReady || FlightSceneManager.instance.switchingScene)
        {
            yield return null; // tells the routine to wait a frame before resuming
        }
        // your code here
        yield break; // stops the routine, this is required at the end of the routine if you don't yield anywhere else, but at that point it might be better to make the return type something else
    }
```
Now all that's left to do is link everything together, in your modloaded function if you place the code `VTOLAPI.SceneLoaded += yourSceneLoadedFunction;`, then whenever the scene is changed your function will be caused, and putting `StartCoroutine(waitFroLoad())` inside the if statement of SceneChanged will cause waitForLoad() to run, now you can run code when the flight scene has loaded. Note that if you want the code to rerun when you will also have to add `VTOLAPI.MissionReloaded += waitForLoead`. 