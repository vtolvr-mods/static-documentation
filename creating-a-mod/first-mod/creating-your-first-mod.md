Now we have downloaded the correct software needed to create mods, we can start making them! 
# Creating the project

Because we have downloaded Danku's mod template, we can use Visual Studio to create a new project.

Open Visual Studio and press "Create a new project"

![Visual Studio start screen with mouse over create a new project](/images/creating-a-mod/vs-create-new-project.png)

Search for "VTOL" in the search bar and you should see the template as the only option.

![Image of Visual Studio, creating a new project menu with the VTOL Mod Selected](/images/creating-a-mod/vtol-mod-template-in-vs.png)

Now you can put in your Project Name and where you would like it to be saved on your PC. In the image below, I am creating a mod called `TutorialMod` and saving it in my code folder.

![Image of configuring a new project in visual studio](/images/creating-a-mod/vs-configure-project-name.png)

Pressing "Next" on the bottom left will lead us to Danku's settings we need to fill out. 

Going from top to bottom:

**Framework** - This kind of what version of C# APIs you target. If you're new to C# just leave this as the default value

**Harmony ID** (Can be changed later) - This is a ID for this Mod, it needs to be unique as if it clashes with another mod it could cause issues. The recommend format is name.modname

**VTOL VR Path** - This is where your VTOL VR is installed. The template will use this to reference the game's and mod loader's DLLs

**Mod Title** (Can be changed later) - This is the title for your mod while you're developing it locally

**Mod Description** (Can be changed later) - This is the description for your mod while you're developing it locally

**Mod Image URL** (Can be changed later) - This is the image URL for your mod while you're developing it locally

> [!IMPORTANT]
> Make sure the VTOL VR Path is correct as it can be a pain if you're new to C# to fix later down the line

![Image of settings the values in Daknu's mod template](/images/creating-a-mod/vs-filling-out-mod-template.png)

Now pressing `Control + B` should build your mod successfully

![Image of building the project in Visual Studio](/images/creating-a-mod/vs-building-project.png)

# Testing our first mod

Now when you launch the mod loader, you should see your new mod at the top!

![Image of the Mod Loader with the mod you created at the top](/images/creating-a-mod/mod-loader-seeing-your-mod.png)

The name, description and image are populated from the `item.json` file inside of your Visual Studio project. Editing that file and building again will update it.

> [!TIP]
> Using [Flatscreen](https://steamcommunity.com/sharedfiles/filedetails/?id=3262379112) and [TriquetraInput](https://steamcommunity.com/sharedfiles/filedetails/?id=3262378482) can help you test your mods without putting on your VR headset

Launching the game, you should now see when you load your mod the default awake log message

![Image of the games log, with the default "Awake at" message](/images/creating-a-mod/game-log-seeing-awake-message.png)