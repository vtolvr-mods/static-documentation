# Releasing your mod on the workshop
Now that you have built and tested your first mod, you can upload it to the Steam workshop. In your Steam library you should see a tool called "VTOL VR Mod Uploader", this program is what you'll use to upload and update items on the workshop.

![Image of the steam library searching VTOL VR](/images/creating-a-mod/steam-mod-uploader.png)

![Image of the uploader with the details filled in](/images/creating-a-mod/mod-uploader.png)


This is the information the players will also see now. 

**Title** - This is the name of your mod

**Folder** - This is the directory that will be uploaded to the workshop. When pressing build, it will copy your mod into the games folder. `steamapps\common\VTOL VR\@Mod Loader\Mods\TutorialMod` All you need is the DLL to upload it to the workshop.

**Thumbnail Image** - This is what shows in game and browsing the Steam workshop. Steam have a 1MB limit in place for this file

**Visibility** - This is who can see your mod on the workshop

**DLL Name** - This tells the mod loader what DLL to load when the user presses load. Not every mod might need a DLL set.

**Allow Load On Start** - Players have the choice if they want a mod to always be loaded, this has caused issues in the past. So this option allows you, the mod developer, to block the ability to allow players to enable load on start.

**Show On Main Menu** - If this is false, the mod will not show up in game. This could be used for an API that doesn't have any use, but other mods depend on it.

**Description** - Description for the workshop page and also shows in game. It uses [Steams custom formatting](https://steamcommunity.com/comment/Guide/formattinghelp)

# Does your mod need any dependencies?

## Local Development

Inside the `item.json` file, you will need to add a `DependenciesIds` key with an array of SteamIds. For example
[!code-json[](item.json?highlight=11-13)]

## Released on steam

Once you release it on Steam, you need to use the web UI to add a dependency. Open the item up in the workshop and press "Add/Remove Required Items"

![Image of the steam workshop page added a required item](/images/creating-a-mod/adding-dependency.png)

This will bring you to a page where you add Add or remove the dependencies. Once they are added, all users should see them as required items when viewing your mod

![Image of a item showing the required items](/images/creating-a-mod/required-items-in-steam.png)