# Software

- Visual Studio with .net desktop development workload
- Danku's New Project Template
- Unity (Optional)
- dnSpy (Optional)

# Visual Studio
You can download Visual Studio for free from the Microsoft website , when installing make sure to include the ".NET desktop development".

![](/images/creating-a-mod/netdesktopdevelopmentworkload.PNG)

This should give you access to create a Class Library in the .NET framework later on. 

![](/images/creating-a-mod/ClassLibrary.PNG)

# Danku's new project template

A community member, Danku, has created a template people can use to create new mods quickly. Once Visual Studio has finished installing, by pressing `Win + R` and typing `cmd` inside the windows run window

![Image of windows run window with cmd typed in](/images/creating-a-mod/windows-run.png)

In this new window, copy and paste the following command:
```
dotnet new install Danku.VTOLVRModLoader.Templates
```

This should install a new project template that we can use in Visual Studio later on

![Image of Visual Studio, creating a new project menu with the VTOL Mod Selected](/images/creating-a-mod/vtol-mod-template-in-vs.png)

# Unity (Optional)

You don't need Unity, however, if you want to start importing your own assets into the game then you will need to create asset bundles inside of Unity. In the `player.log` file, you can see what Unity version VTOL VR currently uses. It will be on the third line EG: `Initialize engine version: 2020.3.30f1 (1fb1bf06830e)`.

You can download all Unity version from [their website here](https://unity3d.com/get-unity/download/archive).

![](/images/creating-a-mod/unity.PNG)

Once these are installed, we can next move onto creating a basic mod in Visual Studio.

# dnSpy (Optional)

dnSpy is what allows you to look at decompiled code and look at what everything does. It's downloadable [here](https://github.com/dnSpyEx/dnSpy/releases/latest).