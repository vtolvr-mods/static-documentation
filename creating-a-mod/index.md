>[!NOTE]
> If you feel like this guide is missing something or has some incorrect information. Feel free to let us know on the discord server.
>
> If you know how to fix the issue, feel free to improve these docs with a merge request [here](https://gitlab.com/vtolvr-mods/static-documentation/-/tree/Production)!

# Creating your first mod!
This text guide will show you the up to date method on how to create a basic mod for VTOL VR using the mod loader at vtolvr-mods.com.

In this guide, we will go through the process of installing the software you need, creating the project, writing the code and testing it in the game.

# Overall Process

Modding in VTOL VR isn't supported by the developer, so there are a few differences to native modding games. Mods for VTOL VR consist of a Class Library in the .NET Framework. The Mod Loader searches these Class Libraries for a class called `VtolMod`. Then once that is found, it will create a new GameObject with do not destroy on load set to it (so it persists between scenes) and adds your `VtolMod` class onto that. Users can also unload your mod, the method `UnLoad()` will get called when this happens.

# More Questions?

If you have any more questions about the process of creating a mod . Here are some useful places to get answers.

## [VTOL VR Modding Discord](https://discord.gg/XZeeafp)

The modding Discord is a great place to get specific questions answered. You can chat with current mod creators and with the general community who like playing with mods. 

We are small though so don't feel ignored if you don't get a response right away.

## [Unity Documentation](https://docs.unity3d.com/Manual/index.html)

If you haven't guessed already, VTOL VR is created in Unity, the Unity Docs are well documented explaining what each function does and how to use it.

You can find VTOL VR's current Unity version by right clicking on the exe Properties > Details and under "Product Version" is the Unity Version. (Currently at the time of writing, it is on 2020.3.48f)

## [HarmonyX Documentation](https://github.com/BepInEx/HarmonyX/wiki)

If you want to tweak existing code like stopping a method in the game from running or fetching a private variable, HarmonyX has got you covered!

HarmonyX is a fork of [Harmony 2](https://github.com/pardeike/Harmony), so most of the documentation is the same however there is some extra improvements HarmonyX brings.