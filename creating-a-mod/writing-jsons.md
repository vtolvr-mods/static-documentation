# JSON Files
Json files follow a format that goes `"Key" : "Value",`, the ModLoader reads these files in order to know how to interpret the mod and for easy modification of descriptions. This is all supported in the ModLoader's mod creator section, but if you want to manually edit the files you can also do it.

Let's look at an example, from the mod All Equips All Time.
![](/images/creating-a-mod/json-example.png)

Not all values shown here were written by its creator, but rather by the website after uploading it. There are only a couple values that matter in the JSON, the website will write the rest for you.

# description
This is self explanatory, what your mod does. 
# dll_file
This is what the ModLoader will inject, if you have multiple dll files this is the file that contains your `VTOLMOD` class.
# name
This is the name of your mod.
# version
This is the version of your mod.

Note that most of these fields will be created and editable in the ModLoader by using Create A Project. If you want an image to be present in your mod, drop it into the mod folder and name it "preview". It's recommended that the file isn't too big, or the mod might take longer to load. 