# ItemId 
Assembly: ModLoader.Framework.dll

Namespace: ModLoader.Framework

Inherits from: System.Attribute

# Description

This is the attribute that you put on your [VtolMod](VtolMod.md) class. This string needs to be unique between all mods, because it is used as the harmony patch ID.

[Source Code](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/dev-v6/Mod%20Loader.Framework/Attributes/ItemId.cs?ref_type=heads)

# Constructors

```cs
public ItemId(string uniqueValue)
```
# Properties

| | |
| --- | --- |
| UniqueValue | The value that is used when creating your harmony patch |


# Example

[!code-csharp[](vtolmod-example.cs?highlight=6)]