# VtolMod 
Assembly: ModLoader.Framework.dll

Namespace: ModLoader.Framework

Inherits from: UnityEngine.MonoBehaviour

# Description

This is the class you need to inherit so that the Mod Loader can spawn your mod. You will also need the attribute [ItemId](ItemId.md)

[Source Code](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/dev-v6/Mod%20Loader.Framework/VtolMod.cs?ref_type=heads)

# Methods

| | |
| --- | --- |
| UnLoad | Called when the user Unloads your mod |

# Example

[!code-csharp[](vtolmod-example.cs)]