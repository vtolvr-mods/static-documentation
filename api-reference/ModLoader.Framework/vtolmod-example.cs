using ModLoader.Framework;
using ModLoader.Framework.Attributes;

namespace Docs;

[ItemId("docs.vtolmod-example")]
public class Main : VtolMod
{
    // Gets called when your mod is loaded
    public void Awake()
    {
        Debug.Log("Hello World!");
    }

    // Gets called when your mod is unloaded
    public override void UnLoad()
    {
        Debug.Log("Bye World!");
    }
}