# SteamQueries
Assembly: ModLoader.dll

Namespace: ModLoader.SteamQuery

Inherits from: UnityEngine.MonoBehaviour

# Description

SteamQueries is used when you want to get some information from the Mod Loader's workshop, not VTOL VR's workshop. This is an **expensive** operation. Each call makes a TCP request to the console app running in the background, which then makes the steam request and then a TCP response. 

This class attempts to use [UniTask](https://github.com/Cysharp/UniTask), which is a async implementation for Unity games to reduce the freezing on the player. The ModLoader has this compiled directly into it, so no need to reference it.

[Source Code](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/dev-v6/ModLoader/SteamQuery/SteamQueries.cs?ref_type=heads)

# Methods
| | |
| --- | --- |
| GetSubscribedItems | Returns all the workshop items the player has subscribed to |
| GetItem | Returns the one specified workshop item, even if they are not subscribed to it |
| GetLoadOnStartSettings | Returns what items the player has on load on start |
| DownloadItem | Starts the download of a workshop item |

# Example

[!code-csharp[](SteamQueries-Example.cs)]