# ModLoader 
Assembly: ModLoader.dll

Namespace: ModLoader

Inherits from: UnityEngine.MonoBehaviour

# Description
This can be used to fetch the locally developed mods by the current player. These mods are located in the game folder at ``VTOL VR\@Mod Loader\Mods``

[Source Code](https://gitlab.com/vtolvr-mods/ModLoader/-/blob/dev-v6/ModLoader/ModLoader.cs?ref_type=heads)

# Methods

| | |
| --- | --- |
| FindLocalItems | Returns the local mods the user has located in `VTOL VR\@Mod Loader\Mods` |

# Example

[!code-csharp[](ModLoader-Example.cs)]