using ModLoader.SteamQuery;

namespace Docs;

[ItemId("docs.steamquery-example")]
public class Main : VtolMod
{
    private ModLoader.SteamQuery.SteamQueries _steamQueries;
    public async UniTaskVoid Awake()
    {
        _steamQueries = GameObject.FindObjectOfType<ModLoader.SteamQuery.SteamQueries>();
        await GetSubscribedItemsExample();
        await GetItemExample();
        await GetLoadOnStartSettingsExample();
        await DownloadItemExample();
    }

    private async UniTask GetSubscribedItemsExample()
    {
        var currentPage = 1;
        const int maxPages = 100;

        while (true)
        {
            if (currentPage > maxPages)
            {
                // Just stopping it if it goes too far
                break;
            }

            var pageResults = await _steamQueries.GetSubscribedItems(currentPage);
            if (pageResults == null)
            {
                break;
            }

            if (!pageResults.HasValues)
            {
                Debug.LogWarning("Get Subscribed Items didn't have any values");
                break;
            }

            foreach (var item in pageResults.Items)
            {
                Debug.Log(item.Title);
            }

            currentPage++;
        }
    }

    private async UniTask GetItemExample()
    {
        var response = await _steamQueries.GetItem(3260883756);
        if (!response.HasValues)
        {
            Debug.LogError("Could not find the item");
            return;
        }

        foreach (var item in response.Items)
        {
            Debug.Log(item.Title);
        }
    }

    private async UniTask GetLoadOnStartSettingsExample()
    {
        var settings = await _steamQueries.GetLoadOnStartSettings();

        foreach (var workshopItem in settings.Data.WorkshopItems)
        {
            Debug.Log($"{workshopItem.Key}={workshopItem.Value}");
        }
        
        foreach (var localItem in settings.Data.LocalItems)
        {
            Debug.Log($"{localItem.Key}={localItem.Value}");
        }
    }

    private async UniTask DownloadItemExample()
    {
        // Will wait for the download to finish
        await _steamQueries.DownloadItem(3260883756);
    }

    public override void UnLoad()
    {
        
    }
}
