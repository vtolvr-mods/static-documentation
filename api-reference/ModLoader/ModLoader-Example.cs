using ModLoader;

namespace Docs;

[ItemId("docs.modloader-example")]
public class Main : VtolMod
{
    public void Awake()
    {
        foreach (var item in ModLoader.Instance.FindLocalItems())
        {
            // Prints the names of all the local mods to the logs
            Debug.Log(item.Title);
        }
    }

    public override void UnLoad() { }
}
