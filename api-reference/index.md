# Where did VTOL API go?

The Mod Loader is shifting towards having API's as standalone mods. To make the main product smaller and have less responsibilities.

If you want an API like library to use, [VTOLAPI](https://steamcommunity.com/sharedfiles/filedetails/?id=3265689427&) does exist on the workshop. These docs are only for the limited public interfaces you could be using in the Mod Loader directly.